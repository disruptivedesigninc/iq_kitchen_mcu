
/* This file is generated by glib-mkenums, do not modify it. This code is licensed under the same license as the containing project. Note that it links to GLib, so must comply with the LGPL linking clauses. */

#ifndef __DZL_ENUMS_H__
#define __DZL_ENUMS_H__

#include <glib-object.h>

#include "dzl-version-macros.h"

G_BEGIN_DECLS

/* enumerations from "dzl-file-transfer.h" */
DZL_AVAILABLE_IN_ALL GType dzl_file_transfer_flags_get_type (void);
#define DZL_TYPE_FILE_TRANSFER_FLAGS (dzl_file_transfer_flags_get_type ())

/* enumerations from "dzl-tree-types.h" */
DZL_AVAILABLE_IN_ALL GType dzl_tree_drop_position_get_type (void);
#define DZL_TYPE_TREE_DROP_POSITION (dzl_tree_drop_position_get_type ())
G_END_DECLS

#endif /* __DZL_ENUMS_H__ */

/* Generated data ends here */

