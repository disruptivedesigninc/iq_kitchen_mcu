#ifndef __RESOURCE_dzl_H__
#define __RESOURCE_dzl_H__

#include <gio/gio.h>

G_GNUC_INTERNAL GResource *dzl_get_resource (void);
#endif
