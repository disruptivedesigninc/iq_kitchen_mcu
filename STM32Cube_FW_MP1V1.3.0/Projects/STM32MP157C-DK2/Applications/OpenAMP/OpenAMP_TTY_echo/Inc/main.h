/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file    OpenAMP/OpenAMP_TTY_echo/Inc/main.h
  * @author  MCD Application Team
  * @brief   Header for main.c module
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2019 STMicroelectronics. 
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the 
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32mp1xx_hal.h"
#include "openamp.h"
#include "lock_resource.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "openamp_log.h"
/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define DEFAULT_IRQ_PRIO      1U
#define STEP_M4_Pin GPIO_PIN_13
#define STEP_M4_GPIO_Port GPIOE
#define STEP_M3_Pin GPIO_PIN_11
#define STEP_M3_GPIO_Port GPIOE
#define STEP_M2_Pin GPIO_PIN_7
#define STEP_M2_GPIO_Port GPIOI
#define STEP_M1_Pin GPIO_PIN_5
#define STEP_M1_GPIO_Port GPIOI
#define LCD_DIMM_PWM_Pin GPIO_PIN_10
#define LCD_DIMM_PWM_GPIO_Port GPIOB
#define BOARD_I2C_SDA_Pin GPIO_PIN_15
#define BOARD_I2C_SDA_GPIO_Port GPIOF
#define MOD_STEP_Pin GPIO_PIN_6
#define MOD_STEP_GPIO_Port GPIOF
#define MOD_CURR_LIM_Pin GPIO_PIN_7
#define MOD_CURR_LIM_GPIO_Port GPIOF
#define BOARD_I2C_SCL_Pin GPIO_PIN_14
#define BOARD_I2C_SCL_GPIO_Port GPIOF
/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
