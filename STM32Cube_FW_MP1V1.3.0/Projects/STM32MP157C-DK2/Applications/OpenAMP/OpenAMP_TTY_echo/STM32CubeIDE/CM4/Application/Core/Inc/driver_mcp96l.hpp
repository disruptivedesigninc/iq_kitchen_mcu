/*
 * driver_mcp96l.hpp
 *
 *  Created on: Feb. 28, 2021
 *      Author: rbaro
 */

#pragma once

#include "i2c.hpp"

class DriverMCP96L {
public:
	DriverMCP96L(I2C& bus, uint16_t address);
	virtual ~DriverMCP96L();

	void service();

	float ambientTemperature() const;
	float temperature() const;
	bool faultOpenTc() const;
	bool faultShortedTc() const;
	bool noComms() const;


private:
	I2C& bus_;
	uint16_t address_;
	float ambient_;
	float temperature_;
	bool faultOpen_;
	bool faultShort_;
	bool faultNoComms_;

	static constexpr uint8_t STATUS_REG = 0b00000100;
	static constexpr uint8_t TC_CONFIG_REG = 0b00000101;
	static constexpr uint8_t DEVICE_CONFIG_REG = 0b00000110;
	static constexpr uint8_t TC_HOT_REG = 0b00000000;
	static constexpr uint8_t TEMP_REG = 0b00000001;
	static constexpr uint8_t TC_COLD_REG = 0b00000010;



	bool configure();

	bool getTemperature();

	bool getStatus();

	void reset();

	float convertTemp(uint8_t low, uint8_t high);
};


