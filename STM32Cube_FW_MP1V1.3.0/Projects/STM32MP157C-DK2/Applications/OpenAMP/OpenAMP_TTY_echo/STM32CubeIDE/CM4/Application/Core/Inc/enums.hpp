/*
 * enums.hpp
 *
 *  Created on: Jan. 18, 2021
 *      Author: rbaro
 */
#pragma once
#include <stdint.h>


namespace MOTOR {
enum  HOME_DIRECTION {CW = 0, CCW};
enum  class FAULT : uint8_t {NONE = 0, MOTOR, HOME_SWITCH};
enum class STATE : uint8_t {FAULT = 0, INITIALIZE, HOMING, MOVING_TO_POSITION, AT_POSITION, HOMING_COMPLETE};
enum PWM_NUMBER {ONE = 0, TWO, THREE, FOUR, FIVE, TOTAL_NUM};

}

namespace TEMP {
enum TEMP_NUMBER {ONE = 0, TWO, THREE, FOUR, FIVE, SIX, TOTAL_NUM};
enum class FAULT : uint8_t {NONE = 0, COMMS, OPEN, SHORT};
enum class STATE : uint8_t {READY = 0, READING};
}

namespace OUTPUT{
enum TYPE {COOLING_FAN = 0, RELAY_1, RELAY_2, RELAY_3, RELAY_4, NUM_OUTPUTS};
enum STATE{OFF = 0, ON = 1};
}
