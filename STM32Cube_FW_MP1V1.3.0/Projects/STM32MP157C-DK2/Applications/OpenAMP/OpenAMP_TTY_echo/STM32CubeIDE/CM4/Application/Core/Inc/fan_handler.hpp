/*
 * fan_handler.hpp
 *
 *  Created on: Jan. 10, 2021
 *      Author: rbaro
 */

#pragma once
#include "gpio.hpp"
#include "status.hpp"
#include "settings.hpp"

class FanHandler {
public:
	FanHandler(Gpio& fan, const Settings& settings, Status& status);
	virtual ~FanHandler();

	void service();

	bool fanRunning() const;

private:
	Gpio& fan_;
	const Settings& settings_;
	Status& status_;

	void fanOn();
	void fanOff();


};


