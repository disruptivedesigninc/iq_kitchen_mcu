/*
 * Gpio.hpp
 *
 *  Created on: Jan 9, 2021
 *      Author: disruptive
 */

#pragma once

#include "stm32mp1xx_hal.h"

enum gpioType {genericInput, genericOutputPP, genericOutputOD};

class Gpio{
public:
	Gpio(GPIO_TypeDef* GPIOx, uint16_t GPIO_Pin, gpioType type);
	virtual ~Gpio();

	bool state() const;

	void set(bool state);

private:
	GPIO_TypeDef* GPIOx_;
	uint16_t GPIO_Pin_;

};


