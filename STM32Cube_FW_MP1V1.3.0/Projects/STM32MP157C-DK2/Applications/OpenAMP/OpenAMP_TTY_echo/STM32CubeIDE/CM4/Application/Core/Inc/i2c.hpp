/*
 * i2c.hpp
 *
 *  Created on: Jan. 18, 2021
 *      Author: rbaro
 */
#pragma once
#include "stm32mp1xx_hal.h"
#include <array>

class I2C {
public:
	I2C(I2C_HandleTypeDef& handle);
	virtual ~I2C();


	uint16_t readWord(uint8_t deviceAddress, uint8_t registerAddress);
	bool writeWord(uint8_t deviceAddress, uint8_t registerAddress, uint16_t data);
	bool writeByte(uint8_t deviceAddress, uint8_t registerAddress, uint8_t data);
	bool read(uint8_t deviceAddress, uint8_t registerAddress, uint8_t numRegisters);

	static const uint8_t BUFFER_SIZE = 100;
	typedef std::array<uint8_t, BUFFER_SIZE> BUFFER;

	const BUFFER& data();


private:
	I2C_HandleTypeDef& handle_;

	static const uint32_t TIMEOUT = 10;
	BUFFER buffer_;

};


