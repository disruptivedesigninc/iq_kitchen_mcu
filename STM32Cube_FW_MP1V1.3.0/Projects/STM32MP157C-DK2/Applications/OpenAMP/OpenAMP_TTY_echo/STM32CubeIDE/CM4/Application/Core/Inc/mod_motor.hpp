/*
 * MotorDriver.hpp
 *
 *  Created on: Jan 9, 2021
 *      Author: disruptive
 */

#pragma once

#include "stm32mp1xx_hal.h"
#include "gpio.hpp"
#include "settings.hpp"
#include "status.hpp"



class ModMotor {
public:
	ModMotor(const Settings& settings, Status& status, Gpio& pwmOutput, Gpio& enableOutput, Gpio& directionOutput, Gpio& faultInput, MOTOR::PWM_NUMBER motorNumber);
	virtual ~ModMotor();

	// Drive Commands
	void home();

	// Move to 0 to 100% position
	void movePosition(float percent);

	float currentPosition() const;

	MOTOR::FAULT fault() const;
	MOTOR::STATE state() const;


	void service();

	static const uint16_t MAX_STEPS = 1200;
	static const uint16_t OVERHOME_STEPS = 500;
	static const MOTOR::HOME_DIRECTION HOME = MOTOR::CCW;
	static const uint16_t DEADBAND = 1;


private:
	const Settings& settings_;
	Status& status_;
	Gpio& pwmOutput_;
	Gpio& enableOutput_;
	Gpio& directionOutput_;
	Gpio& faultInput_;
	MOTOR::PWM_NUMBER motorNumber_;



	int32_t currentPositionSteps_;
	int32_t requestedPositionSteps_;

	bool moveMotor_;
	bool masterDrive_;
	bool direction = true;
	bool homeComplete = false;


	void moveUp();
	void moveDown();

	void stopMove();

	void fault(MOTOR::FAULT fault);
	void state(MOTOR::STATE state);

};


