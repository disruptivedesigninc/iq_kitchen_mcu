/*
 * status.hpp
 *
 *  Created on: Jan. 10, 2021
 *      Author: rbaro
 */

#pragma once
#include <array>
#include "enums.hpp"
#include <algorithm>
#include <cstring>

struct Status {
    Status(){};

    // Temperatures
    std::array<int16_t, TEMP::TEMP_NUMBER::TOTAL_NUM> ambientTempArr;
    std::array<int16_t, TEMP::TEMP_NUMBER::TOTAL_NUM> temperatureArr;
    std::array<uint8_t, TEMP::TEMP_NUMBER::TOTAL_NUM> tempFaultArr;
    std::array<uint8_t, TEMP::TEMP_NUMBER::TOTAL_NUM> tempStateArr;


    // Motor status
    std::array<uint8_t, MOTOR::PWM_NUMBER::TOTAL_NUM> motorFaultArr;
    std::array<uint8_t, MOTOR::PWM_NUMBER::TOTAL_NUM> motorStateArr;
    std::array<bool, MOTOR::PWM_NUMBER::TOTAL_NUM> motorHomeInputArr;

    // Output status
    std::array<bool, OUTPUT::TYPE::NUM_OUTPUTS> outputArr;

    // Flame Status
    uint8_t flameStatus;


    //v1.0
    uint8_t FW_MAJOR_VERSION = 1;
    uint8_t FW_MINOR_VERSION = 1;
    //V1.0 initial release
    //V1.1 Updated the motor drive range from 20000 to 34000.


    static constexpr uint16_t size() {
        return  (std::tuple_size<decltype (ambientTempArr)>::value * sizeof(ambientTempArr[0])) +
                (std::tuple_size<decltype (temperatureArr)>::value * sizeof(temperatureArr[0])) +
                (std::tuple_size<decltype (tempFaultArr)>::value * sizeof(tempFaultArr[0]))+
                (std::tuple_size<decltype (tempStateArr)>::value * sizeof(tempStateArr[0]))+
                (std::tuple_size<decltype (motorFaultArr)>::value * sizeof(motorFaultArr[0]))+
                (std::tuple_size<decltype (motorStateArr)>::value * sizeof(motorStateArr[0]))+
                (std::tuple_size<decltype (motorHomeInputArr)>::value * sizeof(motorHomeInputArr[0]))+
				(std::tuple_size<decltype (outputArr)>::value * sizeof(outputArr[0]))+
                sizeof(flameStatus) +
				sizeof(FW_MAJOR_VERSION) +
				sizeof(FW_MINOR_VERSION);
    }

    void pack(uint8_t* buffer){

        std::memcpy(buffer, ambientTempArr.data(), ambientTempArr.size() * sizeof(ambientTempArr[0]));
        buffer += ambientTempArr.size() * sizeof(ambientTempArr[0]);


        std::memcpy( buffer, temperatureArr.data(),temperatureArr.size() * sizeof(temperatureArr[0]));
        buffer += temperatureArr.size() * sizeof(temperatureArr[0]);

        std::memcpy( buffer, tempFaultArr.data(), tempFaultArr.size() * sizeof(tempFaultArr[0]));
        buffer += tempFaultArr.size() * sizeof(tempFaultArr[0]);

        std::memcpy( buffer, tempStateArr.data(), tempStateArr.size() * sizeof(tempStateArr[0]));
        buffer += tempStateArr.size() * sizeof(tempStateArr[0]);

        std::memcpy( buffer, motorFaultArr.data(), motorFaultArr.size() * sizeof(motorFaultArr[0]));
        buffer += motorFaultArr.size() * sizeof(motorFaultArr[0]);

        std::memcpy( buffer, motorStateArr.data(), motorStateArr.size() * sizeof(motorStateArr[0]));
        buffer += motorStateArr.size() * sizeof(motorStateArr[0]);

        std::memcpy( buffer, motorHomeInputArr.data(), motorHomeInputArr.size() * sizeof(motorHomeInputArr[0]));
        buffer += motorHomeInputArr.size() * sizeof(motorHomeInputArr[0]);

        std::memcpy( buffer, outputArr.data(), outputArr.size() * sizeof(outputArr[0]));
        buffer += outputArr.size() * sizeof(outputArr[0]);

        std::memcpy(buffer, &flameStatus, sizeof(flameStatus));
        buffer += sizeof(flameStatus);

        std::memcpy(buffer, &FW_MAJOR_VERSION, sizeof(FW_MAJOR_VERSION));
         buffer += sizeof(FW_MAJOR_VERSION);

         std::memcpy(buffer, &FW_MINOR_VERSION, sizeof(FW_MINOR_VERSION));
         // buffer += sizeof(FW_MINOR_VERSION);
    }

    void unpack(uint8_t* buffer){
        std::memcpy(ambientTempArr.data(), buffer, ambientTempArr.size() * sizeof(ambientTempArr[0]));
        buffer += ambientTempArr.size() * sizeof(ambientTempArr[0]);


        std::memcpy(temperatureArr.data(), buffer, temperatureArr.size() * sizeof(temperatureArr[0]));
        buffer += temperatureArr.size() * sizeof(temperatureArr[0]);

        std::memcpy(tempFaultArr.data(), buffer, tempFaultArr.size() * sizeof(tempFaultArr[0]));
        buffer += tempFaultArr.size() * sizeof(tempFaultArr[0]);

        std::memcpy(tempStateArr.data(), buffer, tempStateArr.size() * sizeof(tempStateArr[0]));
        buffer += tempStateArr.size() * sizeof(tempStateArr[0]);

        std::memcpy(motorFaultArr.data(), buffer, motorFaultArr.size() * sizeof(motorFaultArr[0]));
        buffer += motorFaultArr.size() * sizeof(motorFaultArr[0]);

        std::memcpy(motorStateArr.data(), buffer, motorStateArr.size() * sizeof(motorStateArr[0]));
        buffer += motorStateArr.size() * sizeof(motorStateArr[0]);

        std::memcpy(motorHomeInputArr.data(), buffer, motorHomeInputArr.size() * sizeof(motorHomeInputArr[0]));
        buffer += motorHomeInputArr.size() * sizeof(motorHomeInputArr[0]);

        std::memcpy(outputArr.data(), buffer, outputArr.size() * sizeof(outputArr[0]));
        buffer += outputArr.size() * sizeof(outputArr[0]);

        std::memcpy(&flameStatus,  buffer, sizeof(flameStatus));
        buffer += sizeof(flameStatus);

        std::memcpy(&FW_MAJOR_VERSION,  buffer, sizeof(FW_MAJOR_VERSION));
        buffer += sizeof(FW_MAJOR_VERSION);

        std::memcpy(&FW_MINOR_VERSION,  buffer, sizeof(FW_MINOR_VERSION));
        //buffer += sizeof(FW_MINOR_VERSION);
    }
};


