#pragma once
#include "status.hpp"
#include "i2c.hpp"
#include "driver_mcp96l.hpp"
#include <array>
#include "enums.hpp"

class TemperatureHandler {
public:
	TemperatureHandler(I2C& bus, Status& status);
	virtual ~TemperatureHandler();

	void service();


private:
	std::array<DriverMCP96L, TEMP::TOTAL_NUM> tempChips_;
	Status& status_;
	uint32_t lastTick_;
	uint8_t currentChip_;


	static constexpr uint16_t ADDR_1 = 0b11000000;
	static constexpr uint16_t ADDR_2 = 0b11001110;
	static constexpr uint16_t ADDR_3 = 0b11001000;
	static constexpr uint16_t ADDR_4 = 0b11000010;
	static constexpr uint16_t ADDR_5 = 0b11001010;
	static constexpr uint16_t ADDR_6 = 0b11001100;

	// This is the maximum time for each to be serviced. AKA divided by the total number of chips.
	static constexpr uint32_t SERVICE_TIME_MS = 1000;

	void updateStatus();

	TEMP::FAULT fault(DriverMCP96L& chip);

};

