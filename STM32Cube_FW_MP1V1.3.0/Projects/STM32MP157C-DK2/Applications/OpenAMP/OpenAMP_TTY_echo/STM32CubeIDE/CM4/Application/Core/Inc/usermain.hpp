#pragma once

#include <functional>
#include "status.hpp"
#include "settings.hpp"

void userMain(std::function<void()> callback, Status& status, Settings& settings);


