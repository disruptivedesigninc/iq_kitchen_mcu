
#pragma once

#include <cmath>

template <typename T>
T gate(T value, T min, T max){
	T minValue = (value < min)? min : value;
	return (value > max)? max : minValue;
}
