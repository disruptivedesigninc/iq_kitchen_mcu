/*
 * driver_mcp96l.cpp
 *
 *  Created on: Feb. 28, 2021
 *      Author: rbaro
 */

#include "driver_mcp96l.hpp"

DriverMCP96L::DriverMCP96L(I2C& bus, uint16_t address) :
bus_(bus),
address_(address),
ambient_(0.0f),
temperature_(0.0f),
faultOpen_(false),
faultShort_(false),
faultNoComms_(false)

{

	reset();
}

DriverMCP96L::~DriverMCP96L() {

}

bool DriverMCP96L::configure() {

// Set to all defaults for now.
	return true;

	// See data sheet
	// Write thermocouple config
//	uint16_t data = 0b00000010;

	// -- all defaults
//	if(bus_.write(address_, TC_CONFIG_REG, data)){
//
//	// Write Device config
//		data = 0b00000000;
//
//		return bus_.write(address_, DEVICE_CONFIG_REG, data);
//	}
//	return false;
}

bool DriverMCP96L::getTemperature() {

	if(bus_.read(address_, TC_HOT_REG, 2)){

		uint8_t low = bus_.data().at(1);
		uint8_t high = bus_.data().at(0);

		temperature_ = convertTemp(low, high);

		if(bus_.read(address_, TC_COLD_REG, 2)){

				uint8_t low = bus_.data().at(1);
				uint8_t high = bus_.data().at(0);

				ambient_ = convertTemp(low, high);
			return true;
		}
	}

	return false;
}

void DriverMCP96L::service() {
	// Reset all data
	reset();
	// C++ function short circuit. Yes, thats how this is suppose to be.
	faultNoComms_ = !(configure() && getStatus() && getTemperature());

}

float DriverMCP96L::ambientTemperature() const {
	return ambient_;
}

float DriverMCP96L::temperature() const {
	return temperature_;
}

bool DriverMCP96L::faultOpenTc() const {
	return faultOpen_;
}

bool DriverMCP96L::faultShortedTc() const {
	return faultShort_;
}

bool DriverMCP96L::noComms() const {
	return faultNoComms_;
}

bool DriverMCP96L::getStatus() {
	if(bus_.read(address_, STATUS_REG, 1)){

		volatile uint8_t data = bus_.data().at(0);

		faultOpen_ =  static_cast<bool>(data & 0b00010000);
		faultShort_ = static_cast<bool>(data & 0b00100000);
		return true;
	}

	return false;
}

void DriverMCP96L::reset() {
	// Reset everything but comms
	ambient_ = 1350.0f;
	temperature_ = 1350.0f;
	faultOpen_ = true;
	faultShort_ = true;
	faultNoComms_ = true;
}

float DriverMCP96L::convertTemp(uint8_t low, uint8_t high) {

	bool sign = static_cast<bool>(high & 0b10000000);
	float temp = (high*16) + (static_cast<float>(low)/16.0f);
	if(sign){
		// Below 0
		temp = temp - 4096;
	}
	return temp;
}
