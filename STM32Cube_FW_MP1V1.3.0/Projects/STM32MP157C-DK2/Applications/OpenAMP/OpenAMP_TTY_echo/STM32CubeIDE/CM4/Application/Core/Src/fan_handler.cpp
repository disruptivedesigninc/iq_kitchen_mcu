/*
 * fan_handler.cpp
 *
 *  Created on: Jan. 10, 2021
 *      Author: rbaro
 */

#include <fan_handler.hpp>

FanHandler::FanHandler(Gpio& fan, const Settings& settings, Status& status):
	fan_(fan),
	settings_(settings),
	status_(status) {

	// Turn fan off on boot
	fanOff();

}

FanHandler::~FanHandler() {
}

void FanHandler::service() {

	// Handle settings not being updated yet from UI with default values.
	uint8_t fanSetpoint = (settings_.fanOnSetpoint == 0)? (40) : settings_.fanOnSetpoint;
	uint8_t deadband = (settings_.fanDeadband == 0)? (5) : settings_.fanDeadband;

	// First check if ANY valid ambient temperature is above the fanOnSetpoint
	bool isAmbientTempTooHigh = false;
	for(int i=0; i<TEMP::TOTAL_NUM; i++){
		float ambientTemp = status_.ambientTempArr[i] / 10.0f;
		if(		(status_.tempFaultArr[i] == static_cast<uint8_t>(TEMP::FAULT::NONE)) &&
				(ambientTemp < 1000) &&
				(ambientTemp > fanSetpoint)){
			isAmbientTempTooHigh = true;
		}
	}

	// Also need to check if ALL valid ambient temperatures are below the deadband
	bool isAmbientTempLowerThanDeadband = true;
	for(int i=0; i<TEMP::TOTAL_NUM; i++){
		float ambientTemp = status_.ambientTempArr[i] / 10.0f;
		if(		(status_.tempFaultArr[i] == static_cast<uint8_t>(TEMP::FAULT::NONE)) &&
				(ambientTemp < 1000) &&
				(ambientTemp > (fanSetpoint - deadband))){
			isAmbientTempLowerThanDeadband = false;
		}
	}

	// If any ambient temperature is too high, we need to turn on the fan.  Else if we are below the deadband we need
	// to turn off the fan.  Note that this is preconditioned on the fan already in a running / non running state
	if(isAmbientTempTooHigh && !fanRunning()){
		fanOn();
	} else if(isAmbientTempLowerThanDeadband && fanRunning()){
		fanOff();
	} else{
		// this is an empty else -- since it should keep state.  There are two cases here:
		// 1) temperature approaches DB with fan off.  In this case Fan should stay off
		// 2) temperature approaches DB with fan on. In this case  the fan should stay on until the temp goes below DB
	}

}

void FanHandler::fanOn() {
	fan_.set(true);
}

void FanHandler::fanOff() {
	fan_.set(false);
}

bool FanHandler::fanRunning() const {
	return fan_.state();
}
