/*
 * Gpio.cpp
 *
 *  Created on: Jan 9, 2021
 *      Author: disruptive
 */

#include <gpio.hpp>
#include "lock_resource.h"

Gpio::Gpio(GPIO_TypeDef* GPIOx, uint16_t GPIO_Pin, gpioType type): GPIOx_(GPIOx), GPIO_Pin_(GPIO_Pin) {
	// Create Standard Init
	GPIO_InitTypeDef defaultConfig;
	defaultConfig.Pin = GPIO_Pin;
	defaultConfig.Pull = GPIO_NOPULL;
	defaultConfig.Speed = GPIO_SPEED_FREQ_MEDIUM;

	switch(type){
		case gpioType::genericOutputPP:
			defaultConfig.Mode = GPIO_MODE_OUTPUT_PP;
			break;
		case gpioType::genericOutputOD:
			defaultConfig.Mode = GPIO_MODE_OUTPUT_OD;
			break;
		case gpioType::genericInput:
		default:
			defaultConfig.Mode = GPIO_MODE_INPUT;
			break;
	}
	PERIPH_LOCK(GPIOx);
	HAL_GPIO_Init(GPIOx, &defaultConfig);
	PERIPH_UNLOCK(GPIOx);
}

Gpio::~Gpio() {

}

bool Gpio::state() const {
	return static_cast<bool>(HAL_GPIO_ReadPin(GPIOx_, GPIO_Pin_));
}

void Gpio::set(bool state) {
	HAL_GPIO_WritePin(GPIOx_, GPIO_Pin_, (state)? GPIO_PIN_SET: GPIO_PIN_RESET);
}
