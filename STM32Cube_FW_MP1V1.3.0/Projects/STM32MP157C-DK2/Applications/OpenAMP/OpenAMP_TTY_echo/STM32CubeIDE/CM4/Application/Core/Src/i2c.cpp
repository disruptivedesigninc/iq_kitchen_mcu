/*
 * i2c.cpp
 *
 *  Created on: Jan. 18, 2021
 *      Author: rbaro
 */

#include <i2c.hpp>



I2C::I2C(I2C_HandleTypeDef& handle): handle_(handle) {

	  handle_.Instance = I2C1;
	  handle_.Init.Timing = 0x10707DBC;
	  handle_.Init.OwnAddress1 = 0;
	  handle_.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
	  handle_.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
	  handle_.Init.OwnAddress2 = 0;
	  handle_.Init.OwnAddress2Masks = I2C_OA2_NOMASK;
	  handle_.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
	  handle_.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
	  if (HAL_I2C_Init(&handle_) != HAL_OK)
	  {
	    while(1);
	  }

	  GPIO_InitTypeDef GPIO_InitStruct = {0};
	  RCC_PeriphCLKInitTypeDef PeriphClkInit = {0};
	  if(handle_.Instance==I2C1)
	  {
	  /* USER CODE BEGIN I2C1_MspInit 0 */

	  /* USER CODE END I2C1_MspInit 0 */
	  if(IS_ENGINEERING_BOOT_MODE())
	  {
	  /** Initializes the peripherals clock
	  */
	    PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_I2C12;
	    PeriphClkInit.I2c12ClockSelection = RCC_I2C12CLKSOURCE_HSI;
	    if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
	    {
	    	while(1);
	    }

	  }

	    __HAL_RCC_GPIOF_CLK_ENABLE();
	    /**I2C1 GPIO Configuration
	    PF15     ------> I2C1_SDA
	    PF14     ------> I2C1_SCL
	    */
	    GPIO_InitStruct.Pin = GPIO_PIN_15|GPIO_PIN_14;
	    GPIO_InitStruct.Mode = GPIO_MODE_AF_OD;
	    GPIO_InitStruct.Pull = GPIO_NOPULL;
	    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	    GPIO_InitStruct.Alternate = GPIO_AF5_I2C1;
	    HAL_GPIO_Init(GPIOF, &GPIO_InitStruct);

	    /* Peripheral clock enable */
	    __HAL_RCC_I2C1_CLK_ENABLE();
	    /* I2C1 interrupt Init */
	    HAL_NVIC_SetPriority(I2C1_EV_IRQn, 1, 0);
	    HAL_NVIC_EnableIRQ(I2C1_EV_IRQn);
	    HAL_NVIC_SetPriority(I2C1_ER_IRQn, 1, 0);
	    HAL_NVIC_EnableIRQ(I2C1_ER_IRQn);
	  /* USER CODE BEGIN I2C1_MspInit 1 */

	  /* USER CODE END I2C1_MspInit 1 */
	  }



	  /** Configure Analogue filter
	  */
	  if (HAL_I2CEx_ConfigAnalogFilter(&handle_, I2C_ANALOGFILTER_ENABLE) != HAL_OK)
	  {
	    while(1);//Error_Handler();
	  }
	  /** Configure Digital filter
	  */
	  if (HAL_I2CEx_ConfigDigitalFilter(&handle_, 0) != HAL_OK)
	  {
		  while(1);
	  }
}

I2C::~I2C() {

}

uint16_t I2C::readWord(uint8_t deviceAddress, uint8_t registerAddress) {
	read(deviceAddress, registerAddress, 2);

	uint16_t high = buffer_[0];
	uint16_t low = buffer_[1];

	return (high << 8) | low;
}

bool I2C::writeWord(uint8_t deviceAddress, uint8_t registerAddress, uint16_t data) {
	buffer_.fill(0);
	buffer_[0] = (data >> 8) & 0xFF;
	buffer_[1] = (data & 0xFF);
	bool noFault = false;
	if(HAL_I2C_Master_Transmit(&handle_, deviceAddress, &registerAddress,1, TIMEOUT) == HAL_OK){

		noFault = (HAL_I2C_Master_Transmit(&handle_, deviceAddress, &buffer_[0],2, TIMEOUT) == HAL_OK);
	}
	return noFault;
}

bool I2C::read(uint8_t deviceAddress, uint8_t registerAddress,
		uint8_t numRegisters) {
	assert_param(numRegisters < BUFFER_SIZE);
	buffer_.fill(0);
	bool noFault = false;
	// Send address
    if(HAL_I2C_Master_Transmit(&handle_, deviceAddress, &registerAddress,1 , TIMEOUT) == HAL_OK){
	// Get data
    	noFault = (HAL_I2C_Master_Receive(&handle_, deviceAddress, &buffer_[0], numRegisters, TIMEOUT) == HAL_OK);
    }
	return noFault;
}

const I2C::BUFFER& I2C::data() {
	return buffer_;
}

bool I2C::writeByte(uint8_t deviceAddress, uint8_t registerAddress,
		uint8_t data) {
	buffer_.fill(0);
	buffer_[0] = data;
	bool noFault = false;
	if(HAL_I2C_Master_Transmit(&handle_, deviceAddress, &registerAddress,1, TIMEOUT) == HAL_OK){
		noFault = (HAL_I2C_Master_Transmit(&handle_, deviceAddress, &buffer_[0],1, TIMEOUT) == HAL_OK);
	}
	return noFault;
}
