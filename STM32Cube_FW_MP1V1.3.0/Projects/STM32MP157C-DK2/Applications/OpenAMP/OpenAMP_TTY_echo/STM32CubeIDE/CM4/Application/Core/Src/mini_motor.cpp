/*
 * MiniMotor.cpp
 *
 *  Created on: Jan 9, 2021
 *      Author: disruptive
 */

#include <mini_motor.hpp>
#include "utility.hpp"

using namespace MOTOR;



MiniMotor::MiniMotor(const Settings& settings, Status& status, Gpio& pwmOutput, Gpio& enableOutput, Gpio& directionOutput, Gpio& faultInput, Gpio& homeInput, MOTOR::PWM_NUMBER motorNumber, bool masterDrive):
	settings_(settings),
	status_(status),
	pwmOutput_(pwmOutput),
	enableOutput_(enableOutput),
	directionOutput_(directionOutput),
	faultInput_(faultInput),
	homeInput_(homeInput),
	motorNumber_(motorNumber),
	currentPositionSteps_(0),
	requestedPositionSteps_(0),
	moveMotor_(false),
	masterDrive_(masterDrive)
	{

	// Initialize state
	fault(FAULT::NONE);
	state(STATE::INITIALIZE);

}

MiniMotor::~MiniMotor() {
}



void MiniMotor::home() {
	currentPositionSteps_ = MAX_STEPS + OVERHOME_STEPS;
	requestedPositionSteps_ = 0;
	moveDown();
	state(STATE::HOMING);
}

void MiniMotor::movePosition(float percent) {

	if(state() == MOTOR::STATE::HOMING){
		return;
	}

	float gatedPercent = gate(percent, 0.0f, 100.0f) / 100.0f;

	 int32_t newPosition = static_cast<uint32_t>(MAX_STEPS * gatedPercent);

	 if(std::abs(newPosition - currentPositionSteps_) > DEADBAND){
		 requestedPositionSteps_ = newPosition;
			state(STATE::MOVING_TO_POSITION);
	 }
}

float MiniMotor::currentPosition() const {
	return 100.0f * ((MAX_STEPS - currentPositionSteps_) / MAX_STEPS);
}

MOTOR::FAULT MiniMotor::fault() const {
	return static_cast<MOTOR::FAULT>(status_.motorFaultArr[motorNumber_]);
}

MOTOR::STATE MiniMotor::state() const {
	return static_cast<MOTOR::STATE>(status_.motorStateArr[motorNumber_]);
}

void MiniMotor::fault(MOTOR::FAULT fault) {
	 status_.motorFaultArr[motorNumber_] = static_cast<uint8_t>(fault);
}

void MiniMotor::state(MOTOR::STATE state) {
	 status_.motorStateArr[motorNumber_] = static_cast<uint8_t>(state);
}

void MiniMotor::service() {

	// Check for run faults
	if(faultInput_.state()){
		//state(STATE::FAULT);
		fault(FAULT::MOTOR);
	}

	// Update status info
	status_.motorHomeInputArr[motorNumber_] = homeInput_.state();

	// service PWM
	if(moveMotor_){
		pwmOutput_.set(!pwmOutput_.state());
		if(direction == true){
			// move up
			currentPositionSteps_++;
		}else{
			currentPositionSteps_--;
		}
	}


	switch(state()) {

	case STATE::INITIALIZE: {
		fault(FAULT::NONE);
		currentPositionSteps_ = 0;
		requestedPositionSteps_ = 0;
		//stopMove();
		home();
		break;
	}

	case STATE::FAULT: {
		// Hold here until re homed
		currentPositionSteps_ = 0;
		requestedPositionSteps_ = 0;
		stopMove();
		break;
	}

	case STATE::HOMING: {
		STATE nextState = STATE::HOMING;


		if (homeInput_.state() == false){
					// Switch has tripped
			moveMotor_ = false;
			currentPositionSteps_ = 0;
			requestedPositionSteps_ = 0;
			nextState = STATE::HOMING_COMPLETE;
		}else if(requestedPositionSteps_ < currentPositionSteps_){
			// Move towards home
			moveDown();
		}else{
			// Here we can lockout to a fault, but we'll default to a flag only.
			//nextState = STATE::FAULT;
			fault(FAULT::HOME_SWITCH);

			moveMotor_ = false;
			currentPositionSteps_ = 0;
			requestedPositionSteps_ = 0;
			nextState = STATE::HOMING_COMPLETE;
		}


		state(nextState);
		break;
	}

	case STATE::HOMING_COMPLETE: {
		STATE nextState = STATE::HOMING_COMPLETE;
		bool m1Complete =
				(status_.motorStateArr.at(MOTOR::PWM_NUMBER::ONE) == static_cast<uint8_t>(STATE::HOMING_COMPLETE)) ||
				(status_.motorStateArr.at(MOTOR::PWM_NUMBER::ONE) == static_cast<uint8_t>(STATE::AT_POSITION));

		bool m2Complete =(status_.motorStateArr.at(MOTOR::PWM_NUMBER::TWO) == static_cast<uint8_t>(STATE::HOMING_COMPLETE)) ||
						(status_.motorStateArr.at(MOTOR::PWM_NUMBER::TWO) == static_cast<uint8_t>(STATE::AT_POSITION));

		bool m3Complete =(status_.motorStateArr.at(MOTOR::PWM_NUMBER::THREE) == static_cast<uint8_t>(STATE::HOMING_COMPLETE)) ||
						(status_.motorStateArr.at(MOTOR::PWM_NUMBER::THREE) == static_cast<uint8_t>(STATE::AT_POSITION));

		bool m4Complete =(status_.motorStateArr.at(MOTOR::PWM_NUMBER::FOUR) == static_cast<uint8_t>(STATE::HOMING_COMPLETE)) ||
						(status_.motorStateArr.at(MOTOR::PWM_NUMBER::FOUR) == static_cast<uint8_t>(STATE::AT_POSITION));


		if(m1Complete && m2Complete && m3Complete && m4Complete){
			stopMove();
			nextState = STATE::AT_POSITION;

		}
		state(nextState);

		break;
	}
	case STATE::MOVING_TO_POSITION: {
		STATE nextState = STATE::MOVING_TO_POSITION;


			if(requestedPositionSteps_ > (currentPositionSteps_)){
				// Move away from home
				moveUp();
			}else if(requestedPositionSteps_ < (currentPositionSteps_)){
				// Move towards home
				moveDown();
			}else{
				// At position
				stopMove();
				nextState = STATE::AT_POSITION;
			}

		state(nextState);
		break;
	}

	case STATE::AT_POSITION: {
		// Hold here.

		stopMove();
		break;
	}
	}

}

void MiniMotor::moveUp() {
	if(masterDrive_){
		directionOutput_.set(HOME == HOME_DIRECTION::CCW);
		enableOutput_.set(false);
	}
	direction = true;
	moveMotor_ = true;
}

void MiniMotor::moveDown() {
	if(masterDrive_){
		directionOutput_.set(HOME == HOME_DIRECTION::CW);
		enableOutput_.set(false);
	}
	direction = false;
	moveMotor_ = true;
}

void MiniMotor::stopMove() {
	moveMotor_ = false;
	if(masterDrive_){
		enableOutput_.set(true);
	}
}

