/*
 * ModMotor.cpp
 *
 *  Created on: Jan 9, 2021
 *      Author: disruptive
 */

#include <mod_motor.hpp>
#include "utility.hpp"

using namespace MOTOR;

ModMotor::ModMotor(const Settings& settings, Status& status, Gpio& pwmOutput, Gpio& enableOutput, Gpio& directionOutput, Gpio& faultInput, MOTOR::PWM_NUMBER motorNumber):
	settings_(settings),
	status_(status),
	pwmOutput_(pwmOutput),
	enableOutput_(enableOutput),
	directionOutput_(directionOutput),
	faultInput_(faultInput),
	motorNumber_(motorNumber),
	currentPositionSteps_(0),
	requestedPositionSteps_(0),
	moveMotor_(false)
	{

	// Initialize state
	fault(FAULT::NONE);
	state(STATE::INITIALIZE);


}

ModMotor::~ModMotor() {
}



void ModMotor::home() {
	currentPositionSteps_ = MAX_STEPS + OVERHOME_STEPS;
	requestedPositionSteps_ = 0;
	moveDown();
	state(STATE::HOMING);
}

void ModMotor::movePosition(float percent) {
	if(state() == MOTOR::STATE::HOMING){
		return;
	}

	float gatedPercent = gate(percent, 0.0f, 100.0f) / 100.0f;

	if(!homeComplete && ((gatedPercent * 100)< 5.0)){
		home();
	}else if((gatedPercent * 100) >= 5.0){
		homeComplete = false;
	 int32_t newPosition = static_cast<uint32_t>(MAX_STEPS * gatedPercent);

	 if(std::abs(newPosition - currentPositionSteps_) > DEADBAND){
		 requestedPositionSteps_ = newPosition;
			state(STATE::MOVING_TO_POSITION);
	 }
	}
}

float ModMotor::currentPosition() const {
	return 100.0f * ((MAX_STEPS - currentPositionSteps_) / MAX_STEPS);
}

MOTOR::FAULT ModMotor::fault() const {
	return static_cast<MOTOR::FAULT>(status_.motorFaultArr[motorNumber_]);
}

MOTOR::STATE ModMotor::state() const {
	return static_cast<MOTOR::STATE>(status_.motorStateArr[motorNumber_]);
}

void ModMotor::fault(MOTOR::FAULT fault) {
	 status_.motorFaultArr[motorNumber_] = static_cast<uint8_t>(fault);
}

void ModMotor::state(MOTOR::STATE state) {
	 status_.motorStateArr[motorNumber_] = static_cast<uint8_t>(state);
}

void ModMotor::service() {

	// Check for run faults
	if(faultInput_.state()){
		//state(STATE::FAULT);
		fault(FAULT::MOTOR);
	}


	// service PWM
	if(moveMotor_){
		pwmOutput_.set(!pwmOutput_.state());
		if(direction == true){
			// move up
			currentPositionSteps_++;
		}else{
			currentPositionSteps_--;
		}
	}


	switch(state()) {

	case STATE::INITIALIZE: {
		fault(FAULT::NONE);
		currentPositionSteps_ = 0;
		requestedPositionSteps_ = 0;
		//stopMove();
		home();
		break;
	}

	case STATE::FAULT: {
		// Hold here until re homed
		currentPositionSteps_ = 0;
		requestedPositionSteps_ = 0;
		stopMove();
		break;
	}

	case STATE::HOMING: {
		STATE nextState = STATE::HOMING;

		if(requestedPositionSteps_ < currentPositionSteps_){
			// Move towards home
			moveDown();
		}else{
			// At position
			homeComplete = true;
			stopMove();
			nextState = STATE::AT_POSITION;
		}

		state(nextState);
		break;
	}

	case STATE::MOVING_TO_POSITION: {
		STATE nextState = STATE::MOVING_TO_POSITION;


			if(requestedPositionSteps_ > (currentPositionSteps_)){
				// Move away from home
				moveUp();
			}else if(requestedPositionSteps_ < (currentPositionSteps_)){
				// Move towards home
				moveDown();
			}else{
				// At position
				stopMove();
				nextState = STATE::AT_POSITION;
			}

		state(nextState);
		break;
	}

	case STATE::AT_POSITION: {
		// Hold here.

		stopMove();
		break;
	}
	}

}

void ModMotor::moveUp() {

	directionOutput_.set(HOME == HOME_DIRECTION::CW);
	enableOutput_.set(false);
	moveMotor_ = true;
	direction = true;
}

void ModMotor::moveDown() {

	directionOutput_.set(HOME == HOME_DIRECTION::CCW);
	enableOutput_.set(false);

	moveMotor_ = true;
	direction = false;
}

void ModMotor::stopMove() {
	moveMotor_ = false;

	enableOutput_.set(true);

}

