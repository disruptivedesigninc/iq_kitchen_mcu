/*
 * temperature_handler.cpp
 *
 *  Created on: Feb. 28, 2021
 *      Author: rbaro
 */

#include "temperature_handler.hpp"
#include "stm32mp1xx_hal.h"

TemperatureHandler::TemperatureHandler(I2C& bus, Status& status):
tempChips_{
	DriverMCP96L(bus, ADDR_1),
	DriverMCP96L(bus, ADDR_2),
	DriverMCP96L(bus, ADDR_3),
	DriverMCP96L(bus, ADDR_4),
	DriverMCP96L(bus, ADDR_5),
	DriverMCP96L(bus, ADDR_6)},
	status_(status),
	lastTick_(HAL_GetTick()),
	currentChip_(0)
{
	// All reset values on construction go into status table

		updateStatus();
}

TemperatureHandler::~TemperatureHandler() {

}

void TemperatureHandler::updateStatus() {
	for(size_t i = 0; i < tempChips_.size(); i++){
		DriverMCP96L& chip = tempChips_[i];

		status_.ambientTempArr[i] = static_cast<int16_t>(chip.ambientTemperature() * 10);
		status_.temperatureArr[i] = static_cast<int16_t>(chip.temperature() * 10);
		status_.tempFaultArr[i] = static_cast<uint8_t>(fault(chip));
	}
}

void TemperatureHandler::service() {


	if(HAL_GetTick() - lastTick_ >= (SERVICE_TIME_MS/tempChips_.size())){
		lastTick_ = HAL_GetTick();

		// Service a new temp.
		tempChips_[currentChip_].service();
		currentChip_++;
		currentChip_ = currentChip_ % tempChips_.size();

		updateStatus();
	}

}

TEMP::FAULT TemperatureHandler::fault(DriverMCP96L &chip) {

	if(chip.noComms()){
		return TEMP::FAULT::COMMS;
	}else if(chip.faultOpenTc()){
		return TEMP::FAULT::OPEN;
	}else if(chip.faultShortedTc()){
		return TEMP::FAULT::SHORT;
	}

	return TEMP::FAULT::NONE;
}
