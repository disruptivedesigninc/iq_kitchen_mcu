/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file    OpenAMP/OpenAMP_TTY_echo/Inc/main.c
  * @author  MCD Application Team
  * @brief   Main program body.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2019 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "gpio.hpp"
#include "stm32mp1xx_hal.h"
#include "temperature_handler.hpp"
#include "i2c.hpp"
#include "mini_motor.hpp"
#include "mod_motor.hpp"
#include "fan_handler.hpp"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
#define MAX_BUFFER_SIZE RPMSG_BUFFER_SIZE
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
IPCC_HandleTypeDef hipcc;
I2C_HandleTypeDef hi2c1;

/* USER CODE BEGIN PV */
VIRT_UART_HandleTypeDef huart0;
VIRT_UART_HandleTypeDef huart1;

__IO FlagStatus VirtUart0RxMsg = RESET;
uint8_t VirtUart0ChannelBuffRx[MAX_BUFFER_SIZE];
uint16_t VirtUart0ChannelRxSize = 0;

__IO FlagStatus VirtUart1RxMsg = RESET;
uint8_t VirtUart1ChannelBuffRx[MAX_BUFFER_SIZE];
uint16_t VirtUart1ChannelRxSize = 0;
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_IPCC_Init(void);
/* USER CODE BEGIN PFP */
static Status status;
static Settings settings;
void VIRT_UART0_RxCpltCallback(VIRT_UART_HandleTypeDef *huart);
void VIRT_UART1_RxCpltCallback(VIRT_UART_HandleTypeDef *huart);
/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */
/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initialize the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */
    if(IS_ENGINEERING_BOOT_MODE())
  {
    /* Configure the system clock */
    	HAL_RCC_DeInit();
    SystemClock_Config();
  }

  log_info("Cortex-M4 boot successful with STM32Cube FW version: v%ld.%ld.%ld \r\n",
                                            ((HAL_GetHalVersion() >> 24) & 0x000000FF),
                                            ((HAL_GetHalVersion() >> 16) & 0x000000FF),
                                            ((HAL_GetHalVersion() >> 8) & 0x000000FF));
  /* USER CODE END Init */

  /*HW semaphore Clock enable*/
  __HAL_RCC_HSEM_CLK_ENABLE();
  /* IPCC initialisation */
   MX_IPCC_Init();
  /* OpenAmp initialisation ---------------------------------*/
   if(!IS_ENGINEERING_BOOT_MODE()){
	   MX_OPENAMP_Init(RPMSG_REMOTE, NULL);
   }



  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */

  /* USER CODE BEGIN 2 */
  /*
   * Create Virtual UART device
   * defined by a rpmsg channel attached to the remote device
   */
   if(!IS_ENGINEERING_BOOT_MODE()){
	  log_info("Virtual UART0 OpenAMP-rpmsg channel creation\r\n");
	  if (VIRT_UART_Init(&huart0) != VIRT_UART_OK) {
		log_err("VIRT_UART_Init UART0 failed.\r\n");
		Error_Handler();
	  }

	  log_info("Virtual UART1 OpenAMP-rpmsg channel creation\r\n");
	  if (VIRT_UART_Init(&huart1) != VIRT_UART_OK) {
		log_err("VIRT_UART_Init UART1 failed.\r\n");
		Error_Handler();
	  }

	  /*Need to register callback for message reception by channels*/
	  if(VIRT_UART_RegisterCallback(&huart0, VIRT_UART_RXCPLT_CB_ID, VIRT_UART0_RxCpltCallback) != VIRT_UART_OK)
	  {
	   Error_Handler();
	  }
	  if(VIRT_UART_RegisterCallback(&huart1, VIRT_UART_RXCPLT_CB_ID, VIRT_UART1_RxCpltCallback) != VIRT_UART_OK)
	  {
		Error_Handler();
	  }
   }
  /* USER CODE END 2 */


  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOD_CLK_ENABLE();
  __HAL_RCC_GPIOE_CLK_ENABLE();
  __HAL_RCC_GPIOF_CLK_ENABLE();
  __HAL_RCC_GPIOG_CLK_ENABLE();
  __HAL_RCC_GPIOH_CLK_ENABLE();
  __HAL_RCC_GPIOI_CLK_ENABLE();
  __HAL_RCC_GPIOZ_CLK_ENABLE();


	 // LEDs
	 	 //Gpio userLed(GPIOG, GPIO_PIN_3, gpioType::genericOutputPP);
	 	 Gpio redLed(GPIOA, GPIO_PIN_1, gpioType::genericOutputPP);
	 	 Gpio blueLed(GPIOA, GPIO_PIN_7, gpioType::genericOutputPP);
	 	 Gpio greenLed(GPIOG, GPIO_PIN_5, gpioType::genericOutputPP);

	 	 // Home signals
	 	 Gpio m1Input(GPIOZ, GPIO_PIN_4, gpioType::genericInput);
	 	 Gpio m2Input(GPIOD, GPIO_PIN_4, gpioType::genericInput);
	 	 Gpio m3Input(GPIOF, GPIO_PIN_12, gpioType::genericInput);
	 	 Gpio m4Input(GPIOF, GPIO_PIN_8, gpioType::genericInput);

	 	 // Motors Out
	 	 Gpio m1Enable(GPIOI, GPIO_PIN_6, gpioType::genericInput);
	 	 Gpio m2Enable(GPIOF, GPIO_PIN_11, gpioType::genericInput);
	 	 Gpio m3Enable(GPIOH, GPIO_PIN_11, gpioType::genericInput);
	 	 Gpio m4Enable(GPIOB, GPIO_PIN_9, gpioType::genericOutputPP);
	 	 Gpio m0Enable(GPIOE, GPIO_PIN_7, gpioType::genericOutputPP);

	 	 Gpio m1Direction(GPIOE, GPIO_PIN_0, gpioType::genericInput);
	 	 Gpio m2Direction(GPIOG, GPIO_PIN_9, gpioType::genericInput);
	 	 Gpio m3Direction(GPIOE, GPIO_PIN_1, gpioType::genericInput);
	 	 Gpio m4Direction(GPIOD, GPIO_PIN_3, gpioType::genericOutputPP);
	 	 Gpio m0Direction(GPIOE, GPIO_PIN_8, gpioType::genericOutputPP);



//	 	 Gpio m1Enable(GPIOI, GPIO_PIN_6, gpioType::genericOutputPP);
//	 	 Gpio m2Enable(GPIOF, GPIO_PIN_11, gpioType::genericOutputPP);
//	 	 Gpio m3Enable(GPIOH, GPIO_PIN_11, gpioType::genericOutputPP);
//	 	 Gpio m4Enable(GPIOB, GPIO_PIN_9, gpioType::genericOutputPP);
//	 	 Gpio m0Enable(GPIOE, GPIO_PIN_7, gpioType::genericOutputPP);
//
//	 	 Gpio m1Direction(GPIOE, GPIO_PIN_0, gpioType::genericOutputPP);
//	 	 Gpio m2Direction(GPIOG, GPIO_PIN_9, gpioType::genericOutputPP);
//	 	 Gpio m3Direction(GPIOE, GPIO_PIN_1, gpioType::genericOutputPP);
//	 	 Gpio m4Direction(GPIOD, GPIO_PIN_3, gpioType::genericOutputPP);
//	 	 Gpio m0Direction(GPIOE, GPIO_PIN_8, gpioType::genericOutputPP);



	 	 Gpio m1PWM(GPIOI, GPIO_PIN_5, gpioType::genericOutputPP);
	 	 Gpio m3PWM(GPIOI, GPIO_PIN_7, gpioType::genericOutputPP);
	 	 Gpio m2PWM(GPIOE, GPIO_PIN_11, gpioType::genericOutputPP);
	 	 Gpio m4PWM(GPIOE, GPIO_PIN_13, gpioType::genericOutputPP);
	 	 Gpio m0PWM(GPIOF, GPIO_PIN_6, gpioType::genericOutputPP);

	 	 // Motors In
	 	 Gpio m1Fault(GPIOA, GPIO_PIN_6, gpioType::genericInput);
	 	 Gpio m2Fault(GPIOB, GPIO_PIN_7, gpioType::genericInput);
	 	 Gpio m3Fault(GPIOG, GPIO_PIN_8, gpioType::genericInput);
	 	 Gpio m4Fault(GPIOA, GPIO_PIN_4, gpioType::genericInput);
	 	 Gpio m0Fault(GPIOE, GPIO_PIN_12, gpioType::genericInput);



	 	 // Fan
	 	 Gpio fan(GPIOH, GPIO_PIN_10, gpioType::genericOutputPP);

	 	 // Relays
	 	 Gpio relay1(GPIOF, GPIO_PIN_9, gpioType::genericOutputPP);
	 	 Gpio relay2(GPIOH, GPIO_PIN_7, gpioType::genericOutputPP);
	 	 Gpio relay3(GPIOG, GPIO_PIN_4, gpioType::genericOutputPP);
	 	 Gpio relay4(GPIOG, GPIO_PIN_13, gpioType::genericOutputPP);

	 	 // Encoders

	 	 // Only for testing encoder GPIO
//	 	 Gpio leftButton(GPIOF, GPIO_PIN_13, gpioType::genericInput);
//	 	 Gpio rightButton(GPIOC, GPIO_PIN_5, gpioType::genericInput);
//	 	 Gpio sw1A(GPIOD, GPIO_PIN_12, gpioType::genericOutputPP);
//	 	 Gpio sw1B(GPIOB, GPIO_PIN_7, gpioType::genericOutputPP);
//	 	 Gpio sw2A(GPIOC, GPIO_PIN_6, gpioType::genericOutputPP);
//	 	 Gpio sw2B(GPIOB, GPIO_PIN_5, gpioType::genericOutputPP);
//	 	Gpio irq(GPIOB, GPIO_PIN_6, gpioType::genericOutputPP);

	 	 I2C i2c(hi2c1);

	 	 TemperatureHandler temperatures(i2c, status);

	 	 // Motor control
	 	 MiniMotor m1(settings, status, m1PWM, m1Enable, m1Direction, m1Fault, m1Input, MOTOR::PWM_NUMBER::ONE, false);
	 	 MiniMotor m2(settings, status, m2PWM, m2Enable, m2Direction, m2Fault, m2Input, MOTOR::PWM_NUMBER::TWO, false);
	 	 MiniMotor m3(settings, status, m3PWM, m3Enable, m3Direction, m3Fault, m3Input, MOTOR::PWM_NUMBER::THREE, false);
	 	 MiniMotor m4(settings, status, m4PWM, m4Enable, m4Direction, m4Fault, m4Input, MOTOR::PWM_NUMBER::FOUR, true);
	 	 ModMotor m0(settings, status, m0PWM, m0Enable, m0Direction, m0Fault, MOTOR::PWM_NUMBER::FIVE);

	 	 // Cooling fan
	 	 FanHandler fanHandler(fan, settings, status);


	 	static uint32_t lastTick_ = 0;
	 	static uint32_t serviceTimeMS = 1;
	 	static uint32_t currentTick_ = 0;

	 	redLed.set(true);
	 	greenLed.set(true);
	 	blueLed.set(true);

	 	m1Enable.set(true);
	 	m2Enable.set(true);
	 	m3Enable.set(true);
	 	m4Enable.set(true);

	 	relay1.set(false);
	 	relay2.set(false);
	 	relay3.set(false);
	 	relay4.set(false);


  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {

	  currentTick_++;

	  		// Motor Service - Max speed 1ms step
	  	if(HAL_GetTick() - lastTick_ >= serviceTimeMS){
	  			lastTick_ = HAL_GetTick();
	  			m1.service();
	  			m2.service();
	  			m3.service();
	  			m4.service();
	  			m0.service();
	  			greenLed.set(!greenLed.state());
	  	}

	  	// Temperature Service
	  		temperatures.service();

		// Fan service
			fanHandler.service();

			if(fanHandler.fanRunning()){
				blueLed.set(false);
			}else{
				blueLed.set(true);
			}

	  	// Comms service
	  		if(!IS_ENGINEERING_BOOT_MODE()){
	  	    OPENAMP_check_for_message();

	  	    if (VirtUart0RxMsg) {
	  	      VirtUart0RxMsg = RESET;
	  	      settings.unpack(VirtUart0ChannelBuffRx);

	  	      status.pack(VirtUart0ChannelBuffRx);
	  	      VIRT_UART_Transmit(&huart0, VirtUart0ChannelBuffRx, status.size());
	  	    }

	  	    if (VirtUart1RxMsg) {
	  	      VirtUart1RxMsg = RESET;
	  	      VIRT_UART_Transmit(&huart1, VirtUart1ChannelBuffRx, VirtUart1ChannelRxSize);
	  	    }
	  		}

	  		// Handle Simple commands

	  		// Move grill to position
	  		m0.movePosition(settings.firingRate);
	  		m1.movePosition(settings.grillPosition);
	  		m2.movePosition(settings.grillPosition);
	  		m3.movePosition(settings.grillPosition);
	  		m4.movePosition(settings.grillPosition);

	  		if((m0.state() == MOTOR::STATE::MOVING_TO_POSITION) ||
	  				(m1.state() == MOTOR::STATE::MOVING_TO_POSITION) ||
	  				(m2.state() == MOTOR::STATE::MOVING_TO_POSITION) ||
	  				(m3.state() == MOTOR::STATE::MOVING_TO_POSITION) ||
	  				(m4.state() == MOTOR::STATE::MOVING_TO_POSITION)){
	  			greenLed.set(false);
	  		}else{
	  			greenLed.set(true);
	  		}


	  		// Ignite burner
	  		if(settings.flameOn == 0x6C){
	  			relay1.set(true);
	  			relay2.set(true);
	  			relay3.set(true);
	  			relay4.set(true);
	  			status.flameStatus = 0x11;
	  			redLed.set(false);
	  		}else{
	  			status.flameStatus = 0x0;
	  			redLed.set(true);
	  			relay1.set(false);
	  			relay2.set(false);
	  			relay3.set(false);
	  			relay4.set(false);
	  			redLed.set(true);
	  		}

	  		// update outputs
	  		status.outputArr[OUTPUT::TYPE::COOLING_FAN] = fanHandler.fanRunning();
	  		status.outputArr[OUTPUT::TYPE::RELAY_1] = relay1.state();
	  		status.outputArr[OUTPUT::TYPE::RELAY_2] = relay2.state();
	  		status.outputArr[OUTPUT::TYPE::RELAY_3] = relay3.state();
	  		status.outputArr[OUTPUT::TYPE::RELAY_4] = relay4.state();

  }
  /* USER CODE END 3 */
}


/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /**Configure LSE Drive Capability
  */
  HAL_PWR_EnableBkUpAccess();
  __HAL_RCC_LSEDRIVE_CONFIG(RCC_LSEDRIVE_MEDIUMHIGH);

  /**Initializes the CPU, AHB and APB busses clocks
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI|RCC_OSCILLATORTYPE_HSE
                |RCC_OSCILLATORTYPE_LSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_BYPASS_DIG;
  RCC_OscInitStruct.LSEState = RCC_LSE_ON;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = 16;
  RCC_OscInitStruct.HSIDivValue = RCC_HSI_DIV1;

  /**PLL1 Config
  */
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLL12SOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLM = 3;
  RCC_OscInitStruct.PLL.PLLN = 81;
  RCC_OscInitStruct.PLL.PLLP = 1;
  RCC_OscInitStruct.PLL.PLLQ = 1;
  RCC_OscInitStruct.PLL.PLLR = 1;
  RCC_OscInitStruct.PLL.PLLFRACV = 0x800;
  RCC_OscInitStruct.PLL.PLLMODE = RCC_PLL_FRACTIONAL;
  RCC_OscInitStruct.PLL.RPDFN_DIS = RCC_RPDFN_DIS_DISABLED;
  RCC_OscInitStruct.PLL.TPDFN_DIS = RCC_TPDFN_DIS_DISABLED;

    /**PLL2 Config
    */
  RCC_OscInitStruct.PLL2.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL2.PLLSource = RCC_PLL12SOURCE_HSE;
  RCC_OscInitStruct.PLL2.PLLM = 3;
  RCC_OscInitStruct.PLL2.PLLN = 66;
  RCC_OscInitStruct.PLL2.PLLP = 2;
  RCC_OscInitStruct.PLL2.PLLQ = 1;
  RCC_OscInitStruct.PLL2.PLLR = 1;
  RCC_OscInitStruct.PLL2.PLLFRACV = 0x1400;
  RCC_OscInitStruct.PLL2.PLLMODE = RCC_PLL_FRACTIONAL;
  RCC_OscInitStruct.PLL2.RPDFN_DIS = RCC_RPDFN_DIS_DISABLED;
  RCC_OscInitStruct.PLL2.TPDFN_DIS = RCC_TPDFN_DIS_DISABLED;

    /**PLL3 Config
    */
  RCC_OscInitStruct.PLL3.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL3.PLLSource = RCC_PLL3SOURCE_HSE;
  RCC_OscInitStruct.PLL3.PLLM = 2;
  RCC_OscInitStruct.PLL3.PLLN = 34;
  RCC_OscInitStruct.PLL3.PLLP = 2;
  RCC_OscInitStruct.PLL3.PLLQ = 17;
  RCC_OscInitStruct.PLL3.PLLR = 37;
  RCC_OscInitStruct.PLL3.PLLRGE = RCC_PLL3IFRANGE_1;
  RCC_OscInitStruct.PLL3.PLLFRACV = 0x1A04;
  RCC_OscInitStruct.PLL3.PLLMODE = RCC_PLL_FRACTIONAL;
  RCC_OscInitStruct.PLL3.RPDFN_DIS = RCC_RPDFN_DIS_DISABLED;
  RCC_OscInitStruct.PLL3.TPDFN_DIS = RCC_TPDFN_DIS_DISABLED;

    /**PLL4 Config
    */
  RCC_OscInitStruct.PLL4.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL4.PLLSource = RCC_PLL4SOURCE_HSE;
  RCC_OscInitStruct.PLL4.PLLM = 4;
  RCC_OscInitStruct.PLL4.PLLN = 99;
  RCC_OscInitStruct.PLL4.PLLP = 6;
  RCC_OscInitStruct.PLL4.PLLQ = 8;
  RCC_OscInitStruct.PLL4.PLLR = 8;
  RCC_OscInitStruct.PLL4.PLLRGE = RCC_PLL4IFRANGE_0;
  RCC_OscInitStruct.PLL4.PLLFRACV = 0;
  RCC_OscInitStruct.PLL4.PLLMODE = RCC_PLL_INTEGER;
  RCC_OscInitStruct.PLL4.RPDFN_DIS = RCC_RPDFN_DIS_DISABLED;
  RCC_OscInitStruct.PLL4.TPDFN_DIS = RCC_TPDFN_DIS_DISABLED;

  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
  Error_Handler();
  }
  /**RCC Clock Config
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_ACLK
                |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2
                |RCC_CLOCKTYPE_PCLK3|RCC_CLOCKTYPE_PCLK4
                |RCC_CLOCKTYPE_PCLK5|RCC_CLOCKTYPE_MPU;
  RCC_ClkInitStruct.MPUInit.MPU_Clock = RCC_MPUSOURCE_PLL1;
  RCC_ClkInitStruct.MPUInit.MPU_Div = RCC_MPU_DIV2;
  RCC_ClkInitStruct.AXISSInit.AXI_Clock = RCC_AXISSOURCE_PLL2;
  RCC_ClkInitStruct.AXISSInit.AXI_Div = RCC_AXI_DIV1;
  RCC_ClkInitStruct.MCUInit.MCU_Clock = RCC_MCUSSOURCE_PLL3;
  RCC_ClkInitStruct.MCUInit.MCU_Div = RCC_MCU_DIV1;
  RCC_ClkInitStruct.APB4_Div = RCC_APB4_DIV2;
  RCC_ClkInitStruct.APB5_Div = RCC_APB5_DIV4;
  RCC_ClkInitStruct.APB1_Div = RCC_APB1_DIV2;
  RCC_ClkInitStruct.APB2_Div = RCC_APB2_DIV2;
  RCC_ClkInitStruct.APB3_Div = RCC_APB3_DIV2;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct) != HAL_OK)
  {
  Error_Handler();
  }

  /**Set the HSE division factor for RTC clock
  */
  __HAL_RCC_RTC_HSEDIV(24);
}

/**
  * @brief IPPC Initialization Function
  * @param None
  * @retval None
  */
static void MX_IPCC_Init(void)
{

  hipcc.Instance = IPCC;
  if (HAL_IPCC_Init(&hipcc) != HAL_OK)
  {
     Error_Handler();
  }
}

/* USER CODE BEGIN 4 */
void VIRT_UART0_RxCpltCallback(VIRT_UART_HandleTypeDef *huart)
{

    log_info("Msg received on VIRTUAL UART0 channel:  %s \n\r", (char *) huart->pRxBuffPtr);

    /* copy received msg in a variable to sent it back to master processor in main infinite loop*/
    VirtUart0ChannelRxSize = huart->RxXferSize < MAX_BUFFER_SIZE? huart->RxXferSize : MAX_BUFFER_SIZE-1;
    memcpy(VirtUart0ChannelBuffRx, huart->pRxBuffPtr, VirtUart0ChannelRxSize);
    VirtUart0RxMsg = SET;
}

void VIRT_UART1_RxCpltCallback(VIRT_UART_HandleTypeDef *huart)
{

    log_info("Msg received on VIRTUAL UART1 channel:  %s \n\r", (char *) huart->pRxBuffPtr);

    /* copy received msg in a variable to sent it back to master processor in main infinite loop*/
    VirtUart1ChannelRxSize = huart->RxXferSize < MAX_BUFFER_SIZE? huart->RxXferSize : MAX_BUFFER_SIZE-1;
    memcpy(VirtUart1ChannelBuffRx, huart->pRxBuffPtr, VirtUart1ChannelRxSize);
    VirtUart1RxMsg = SET;
}
/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @param  file: The file name as string.
  * @param  line: The line in file as a number.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  log_err("Error_Handler");
  while(1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  log_err("OOOps: file %s, line %d\r\n", __FILE__, __LINE__);
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
