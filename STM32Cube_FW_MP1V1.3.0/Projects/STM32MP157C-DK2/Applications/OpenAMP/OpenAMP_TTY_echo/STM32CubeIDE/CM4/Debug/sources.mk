################################################################################
# Automatically-generated file. Do not edit!
################################################################################

ELF_SRCS := 
C_UPPER_SRCS := 
CXX_SRCS := 
C++_SRCS := 
OBJ_SRCS := 
S_SRCS := 
CC_SRCS := 
C_SRCS := 
CPP_SRCS := 
S_UPPER_SRCS := 
O_SRCS := 
OBJDUMP_LIST := 
C_UPPER_DEPS := 
S_DEPS := 
C_DEPS := 
OBJCOPY_BIN := 
CC_DEPS := 
SIZE_OUTPUT := 
C++_DEPS := 
EXECUTABLES := 
OBJS := 
CXX_DEPS := 
S_UPPER_DEPS := 
CPP_DEPS := 

# Every subdirectory with source files must be described here
SUBDIRS := \
Application/Core/Src \
Application/Startup \
Application/User \
Drivers/BSP \
Drivers/CMSIS \
Drivers/STM32MP1xx_HAL_Driver \
Middlewares/OpenAMP/libmetal \
Middlewares/OpenAMP/libmetal/generic \
Middlewares/OpenAMP/libmetal/generic/cortexm \
Middlewares/OpenAMP/open-amp/remoteproc \
Middlewares/OpenAMP/open-amp/rpmsg \
Middlewares/OpenAMP/open-amp/virtio \
Middlewares/OpenAMP/virtual_driver \

